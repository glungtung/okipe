---
title: "#Occupons Partout!"
featured_image: "images/okipe.jpg"
omit_header_text: true
description: Occupons Partout ! 
menu: main
cover_dimming_class: 100
weight: 5

---

# La méteo des occupations 

A ce jour, les lieux occupés partout en France :

![Carte](/images/carte.jpg)


# Communiqué commun à tous les lieux occupés

![Communiqué](/images/occuponsPartout.jpeg)

# Les vendredis de la colère {#vendredis}

![vendredis de la colère](/images/Tract-Ocupation-Odeon-vendredis-page-002.jpg)
[Lien vers le tract Odéon](/images/Tract-Ocupation-Odeon-vendredis-page-001.jpg)