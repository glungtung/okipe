---
title: "Programme Fabrik"
featured_image: "images/okipe.jpg"
omit_header_text: true
description: Programme 
menu: main
cover_dimming_class: 100
weight: 1

---


Bonjour à toutes et à tous!  
Voici le programme des RonKozé et des actions, nous vous attendons!                    


![programme](/images/programme.png)
## Somèn 2, semaine du 22 mars
                     



### 🔶 Mardi 23 Mars - La Fabrik OKIPE

- 14h : **Mizik an dalonaz**, Jam Session, Scène ouverte 


### 🔶 Merkrodi 24 Mars  - La Fabrik OKIPE

- 15h: Réfleksyon su **l’armontraz (_transmission)_** ek _Serz Dafreville artiste-formateur sanm son lantouraz pintad_


### 🔶 Zédi 25 Mars - La Fabrik OKIPE

- 10h : Sobatkoz, **Agriculture et Culture**, _avec Emilie Magnant_.  
Ce sont 2 secteurs très proches : ils sont essentiels, locaux, en circuit court, engagés.   

- 13h : **Culture à proximité de tous** / kiltir an Métavek èk domoun : Permettre à chacun, dans les hauts et dans les bas, de laisser exprimer sa culture, et de s’ouvrir aux cultures des autres 

Avec _Loran Hoarau historien , Jean François Rebeyrotte - médiateur culturel, Dominique Carrere_


### 🔶 Vandrodi 26 Mars 

**Les vendredis de la colère / lansiv la kolèr** : action nationale commune à tous les lieux occupés (communiquée dès que possible) [lien vers le tract](/occuponspartout/#vendredis)


### 🔶 Samdi 27 Mars

- 13h : **Actions artistiques en extérieur / Vavangads déor** : performance artistique place des droits de l’homme parvis Champ Fleuri, inspirée [de cette action](https://www.facebook.com/antoine.renault84/videos/3897329703683364/) (Les costumes et masques seront fournis si vous prévenez de votre présence à contact@okipe.re avant vendredi)  
Puis **Ron maloya**

### 🔶 Dimans 28 Mars :

- 9h devant la Mairie de St Pierre : **Marche pour le climat** avec des organisations locales : _Citoyens pour le climat, Extinction rebellion, Greenpeace, Alternatiba, QG Zazalé, Attac, FSU, CGTR Spektak, Occupation Réunion 2021_ : 

### 🔶 Tous les jours
**“Le Bureau des Amours”** recense et répertorie toutes les difficultés, propositions, rêves et utopies, tous les jours sur place à La Fabrik Okipé, ou [par mail](mailto:contact@okipe.re)

-------------

![programme](/images/programme.png)
## Programme de la semaine du 15 mars 

### 🔶 Mardi 16 sanm / Merkredi 17 Mars  
- Rankont ek Domoun  
- Rencontres informelles  



### 🔶 Zédi 18 Mars 
#### 10h  
- Ronkozé : Prézantman lokipasyon-la, son fondman sanm son « roklamaz » (Rovandikasyon) Kozman si lapèl nasyonal de l’ODEON  
- Rencontre : Présentation de l’occupation et de l’appel national de L’ODEON  

#### 14h  
- Fonnkèr



### 🔶 Vendrodi 19 Mars

#### 10h
- Réfleksyon : Vyolans (Batay- Ralé-Pousé- Trikmardaz) dann travay  
- Réflexion : violence dans le milieu professionnel  

#### 14h  
- Mizik  



### 🔶 Samdi 20 Mars

#### 10h
- Sobatkoz : Gayardriman lapovté, innot vizyon péyi-la (innot louksiraz pou La Rényon)  
- Débat : Richesse de la pauvreté, une autre vision de La Réunion  

#### 14h
-   Mizik


### 🔶   Dimans 21 Mars 

#### 14h
-  Ron Maloya  
Nou lé la anzourné pou koz zèk zot pou ékout zot lidé, zot santiman, zot gayar, zot rèv…  
Parallèlement à ce programme, nous sommes là toute la journée pour parler avec vous, recueillir vos idées, vos ressentis, vos joies, vos rêves… Alors n'hésitez pas!


