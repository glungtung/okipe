---
title: "Okipé Réunion"
date: 2021-03-14T23:03:13+04:00
draft: false
omit_header_text: true
featured_image: "images/okipe.jpg"
type: page
menu: main
weight : 1
---

# OKIPÉ ! ANSOUKÉ ! AKOZ ? OUSA ? 

![logo](images/logo.png)

Cette semaine, cela fera un an que la première phase de confinement a été mise en place. Au cours de cette année, les différentes mesures d’adaptation à la situation sanitaire que nous avons vu se succéder ont fortement impacté :  
🔸La situation des travailleurs précaires  
🔸Le fonctionnement du secteur culturel  
🔸Et dans un sens plus large, le fonctionnement de notre cadre de vie collectif et de nos libertés individuelles avec les interdictions de regroupement, la restriction de nos déplacements ou encore les obligations liées au port du masque.

Alors que pour l’instant aucun relâchement ne semble se profiler dans la conduite de la politique sanitaire, il nous apparaît aujourd’hui essentiel de partager notre inquiétude, de faire entendre notre colère et nos propositions.  
Quelles seront les répercussions de cette situation si nous basculons dans l’acceptation ? Si nous ne nous attaquons pas rapidement et collectivement à l’élaboration de mesures correctives des inégalités et dysfonctionnements engendrés par la crise sanitaire ?  
En tant qu’acteurs du secteur culturel, pour notre occupation nous avons d’abord recherché un symbole, un lieu, qui par sa labellisation, représente une vitrine tant pour l’Etat que pour les collectivités et les artistes. Un de ces modèles que nous souhaitons questionner de façon plus globale.  
Mais aussi nous avons choisi ce qui pourrait ressembler à une agora, un refuge, une maison du peuple, un lieu à construire pour tous. Alors il nous a semblé naturel de développer le débat au sein du Centre Dramatique National de La Réunion, un lieu qui a été conçu pour que se croisent les publics, les artistes, un lieu pour tous pouvant accueillir la fabrication d’idées nouvelles.
Il est donc bien entendu que notre intention n’est pas de pénaliser le projet qui se développe au sein de la Fabrik mais, au contraire, d’alimenter son volet citoyen.  
Au cours de cette occupation, nous ferons en sorte que le fonctionnement du lieu et l’ensemble des activités qui y étaient programmées puissent se dérouler normalement.  
A l'initiative de la CGTR Spektak, cette occupation s’inscrit en continuité d’un mouvement national #OccupationOdeon. À ce jour (13 mars 2020), 30 lieux culturels sont déjà occupés en France !  

Cette ooccupation a pour objectif de développer une réflexion collective sur les thématiques suivantes :  
🔹Développer la solidarité citoyenne et freiner la montée de la peur  
🔹Partager son ressenti sur l’état de nos  libertés individuelles  
🔹Se mobiliser pour l’indemnisation des chômeurs et l’aide aux travailleurs précaires  
🔹Proposer des pistes d’évolution pour le secteur du spectacle  

Cette liste est provisoire et doit être enrichie de vos propositions, notamment en ce qui concerne la prise en compte de nos spécificités locales.  
Vous pouvez également retrouver ici [les revendications de la CGTR Spektak](/CGTR-SPEKTAK-Communique-Occupation-CDNOI-La-Fabrik-13-03-2021.pdf).

Rejoignez nos ateliers thématiques, Rondkozé, Sobatkoz, Kabar !  
Retrouvons-nous tous les jours à la Fabrik !  
28 rue Léopold Rambaud 97490 Saint-Clotilde  
