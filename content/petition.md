---
title: "Signer la pétition"
featured_image: "images/okipe.jpg"
omit_header_text: true
description: Occupons Partout ! 
menu: main
cover_dimming_class: 100
weight: 4

---

![megaphone](/images/megaphone.png) 

[Appel à signer la pétition!](https://www.change.org/p/cgtr-spektak-okip-out-kiltir-occupation-r%C3%A9union-2021)

Mesdames, Messieurs, Domoun,

Depuis le Samedi 13 Mars 2021, nous occupons La Fabrik, Centre Dramatique National de l'Océan-Indien, à Saint- Denis, Île de La Réunion. Cette action s'inscrit dans un mouvement national de lutte pour les libertés fondamentales et contre la casse du contrat social français.

Nous vous sollicitons aujourd'hui pour nous apporter votre confiance et votre soutien en signant cette pétition.
Ensemble pour une société nouvelle avec pour ambition une politique des «CULTURES» de notre diversité !
Merci à toutes et à tous!

https://www.change.org/p/cgtr-spektak-okip-out-kiltir-occupation-r%C3%A9union-2021
