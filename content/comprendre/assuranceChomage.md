---
title: "Assurance chômage"
draft: false
featured_image: "/images/assurancechomage.jpg"

---

### La vidéo d'explication d'AlloOdéon 
Pour comprendre les enjeux de la réforme de l'assurance-chômage qui doit entrer en vigueur le 1er juillet.
{{< youtube rt7Lv03F3ZY >}}

---- 

### Séminaire en visio pour comprendre l'assurance chômage 
#### avec Denis Gravouil (CGT), Matthieu Grégoire (sociologue), Samuel Churin
{{< facebook-video "https://www.facebook.com/109193377899630/videos/4241712169195226" >}}

