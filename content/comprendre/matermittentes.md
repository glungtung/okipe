---
title: "Les Matermittentes"
draft: false
featured_image: "/images/matermittentes2.jpg"

---

### Le séminaire du 22 mars où nous recevions les matermittentes 
{{< youtube 5A5JCsuqGkE >}}

[Le PDF de présentation utilisé au séminaire](/PrésentationMatermittentesseminaire22mars.pdf)


### Le site des Matermittentes
 https://www.matermittentes.com