---
title: "Comprendre"
featured_image: "images/okipe.jpg"
omit_header_text: true
description: Occupons Partout ! 
cover_dimming_class: 100
weight: 4
---
## Comprendre les différents sujets qui font partie des revendications